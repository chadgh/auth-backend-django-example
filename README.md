An example authentication backend implementation Django project.

See `custom/backends.py` for the custom backend.

Django auth code:

* [`django.contrib.auth.authenticate`](https://github.com/django/django/blob/1.6/django/contrib/auth/__init__.py#L43)
* [`django.contrib.auth.login`](https://github.com/django/django/blob/1.6/django/contrib/auth/__init__.py#L67)
* [`django.contrib.auth.logout`](https://github.com/django/django/blob/1.6/django/contrib/auth/__init__.py#L92)
* [`django.contrib.auth.get_user`](https://github.com/django/django/blob/1.6/django/contrib/auth/__init__.py#L126)
* [`django.contrib.auth.middleware.AuthenticationMiddleware`](https://github.com/django/django/blob/3c447b108ac70757001171f7a4791f493880bf5b/django/contrib/auth/middleware.py#L16)
* [`cas.backends.CASBackend`](https://github.com/kstateome/django-cas/blob/develop/cas/backends.py#L216)
* [Django custom authentication backend](https://docs.djangoproject.com/en/1.10/topics/auth/customizing/#writing-an-authentication-backend)


When a request comes in the AuthenticationMiddleware attaches the current user to the request object.

```
HTTP request
 |
 V
AuthenticationMiddleware - adds user to request (calls auth.get_user)
 |
 | - auth.get_user(request)
 |   |
 |   V
 |   [authentication backend].get_user(user_id)
 |
 |
 V
Django view
```

If the Django view requires authentication or attempts to authenticate the user, the user's credentials are captured and an authentication backend is used to authenticate and return a user instance. Once that is done, the user instance is passed to the login method to set appropriate authentication values. This user is still the user attached to the request object.

```
auth.authenticate(**credentials)
 |
 V
[authentication backend].authenticate(**credentials)
 |
 V
auth.login(request, user) # sets session values
```
