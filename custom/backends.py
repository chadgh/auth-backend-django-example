from django.contrib.auth.models import User


class DumbDatabaseBackend():

    def authenticate(self, username=None):
        """
        Returns user that authenticates given the specified credentials.
        """
        user = None
        if username:
            user, _ = User.objects.get_or_create(username=username)
            user.is_staff = user.is_superuser = True
            user.save()
        return user

    def get_user(self, user_id):
        """
        Return a user given the user_id.
        """
        try:
            user = User.objects.get(pk=user_id)
        except User.DoesNotExist:
            user = None
        return user
