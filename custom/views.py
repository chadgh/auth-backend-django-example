from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import auth
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required


def login(request):
    username = request.GET.get('username')

    if username:
        user = auth.authenticate(username=username)
        auth.login(request, user)
        return HttpResponseRedirect(reverse('index'))

    return HttpResponse(
        """You haven't logged in yet. <br>
        <form action="/login/" method="get">
        <input type="text" name="username">
        <input type="submit" value="Login">
        </form>
        """
    )


def logout(request):
    auth.logout(request)
    return HttpResponseRedirect(reverse('landing'))


@login_required
def index(request):
    return HttpResponse('You made it, logged in as ' + request.user.username + '! <a href="/admin/">Admin</a> OR <a href="/logout/">Logout</a>.')


def landing(request):
    return HttpResponse('Welcome. <a href="/login/">Login</a>.')
